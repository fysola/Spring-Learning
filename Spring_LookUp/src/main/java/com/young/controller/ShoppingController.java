package com.young.controller;

import com.young.service.ShoppingCart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: create by Young
 * @version: v1.0
 * @description: com.young.controller
 * @date:2019/3/22 23:48
 */
@RestController
public class ShoppingController {
	//@Autowired
	private ShoppingCart shoppingCart;

	//ShoppingController是单例的，而ShoppingCart是非单例的，
	//故spring只会为shoppingCart注入一次实例，导致多人使用了同一个购物车
	//Lookup注解会让spring重写下面的getInstance()方法
	//使得每次调用getInstance()，容器都能返回一个新实例
	//相当于每次都调用了 context.getBean("shoppingCart");
	@Lookup
	protected ShoppingCart getInstance() {
		return shoppingCart;
	}

	@GetMapping("/add/{product}")
	public String testAddProduct(@PathVariable(value="product") String product) {
		ShoppingCart shoppingCart = getInstance();
		shoppingCart.addProduct(product);
		return shoppingCart.showProduct();
	}
}
