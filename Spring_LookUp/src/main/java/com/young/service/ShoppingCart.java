package com.young.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

/**
 * @author: create by Young
 * @version: v1.0
 * @description: com.young.service
 * @date:2019/3/22 23:49
 */
@Component("shoppingCart")
@Scope(SCOPE_PROTOTYPE)
public class ShoppingCart {
	private ArrayList<String> products = new ArrayList<>();

	public void addProduct(String product) {
		products.add(product);
	}

	public String showProduct() {
		return products.toString();
	}
}
